package org.dev.util.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.dev.util.entity.SysSequenceQueue;
import org.dev.util.mapper.SysSequenceQueueMapper;
import org.dev.util.service.SysSequenceQueueService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 生成的队列号 服务实现类
 * </p>
 *
 * @author dean.x.liu
 * @since 2020-07-20
 */
@Service
public class SysSequenceQueueServiceImpl extends ServiceImpl<SysSequenceQueueMapper, SysSequenceQueue> implements SysSequenceQueueService {

}

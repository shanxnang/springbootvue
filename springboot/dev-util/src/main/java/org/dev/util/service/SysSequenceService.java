package org.dev.util.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.dev.util.entity.SysSequence;

/**
 * <p>
 * sys_sequence 服务类
 * </p>
 *
 * @author dean.x.liu
 * @since 2020-07-08
 */
public interface SysSequenceService extends IService<SysSequence> {

}

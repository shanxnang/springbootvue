package org.dev.util.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.dev.util.entity.SysLog;

/**
 * <p>
 * 操作记录 Mapper 接口
 * </p>
 *
 * @author dean.x.liu
 * @since 2020-07-06
 */
public interface SysLogMapper extends BaseMapper<SysLog> {

}

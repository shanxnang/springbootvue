package org.dev.util.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.dev.util.entity.SysSequence;

/**
 * <p>
 * sys_sequence Mapper 接口
 * </p>
 *
 * @author dean.x.liu
 * @since 2020-07-08
 */
public interface SysSequenceMapper extends BaseMapper<SysSequence> {

}

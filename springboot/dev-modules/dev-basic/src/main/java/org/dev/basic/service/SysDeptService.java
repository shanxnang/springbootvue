package org.dev.basic.service;


import com.baomidou.mybatisplus.extension.service.IService;
import org.dev.basic.entity.SysDept;

/**
 * <p>
 * 部门表 服务类
 * </p>
 *
 * @author dean.x.liu
 * @since 2020-10-24
 */
public interface SysDeptService extends IService<SysDept> {

}

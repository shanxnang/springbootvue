package org.dev.basic.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.dev.basic.entity.SysDept;

/**
 * <p>
 * 部门表 Mapper 接口
 * </p>
 *
 * @author dean.x.liu
 * @since 2020-10-24
 */
public interface SysDeptMapper extends BaseMapper<SysDept> {

}

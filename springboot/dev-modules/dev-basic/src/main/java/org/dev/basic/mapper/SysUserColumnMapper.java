package org.dev.basic.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.dev.basic.entity.SysUserColumn;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author hlt
 * @since 2020-06-24
 */
public interface SysUserColumnMapper extends BaseMapper<SysUserColumn> {

}

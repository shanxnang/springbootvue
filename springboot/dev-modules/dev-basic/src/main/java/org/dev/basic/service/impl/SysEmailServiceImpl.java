package org.dev.basic.service.impl;

import org.dev.basic.entity.SysEmail;
import org.dev.basic.mapper.SysEmailMapper;
import org.dev.basic.service.SysEmailService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author dean.x.liu
 * @since 2022-01-27
 */
@Service
public class SysEmailServiceImpl extends ServiceImpl<SysEmailMapper, SysEmail> implements SysEmailService {

}

package org.dev.basic.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.dev.basic.entity.SysUserColumn;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author hlt
 * @since 2020-06-24
 */
public interface SysUserColumnService extends IService<SysUserColumn> {

}

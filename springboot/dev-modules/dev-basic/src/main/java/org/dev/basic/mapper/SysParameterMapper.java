package org.dev.basic.mapper;

import org.dev.basic.entity.SysParameter;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 系统参数 Mapper 接口
 * </p>
 *
 * @author dean.x.liu
 * @since 2021-05-19
 */
public interface SysParameterMapper extends BaseMapper<SysParameter> {

}

package org.dev.basic.service.impl;

import org.dev.basic.entity.SysParameter;
import org.dev.basic.mapper.SysParameterMapper;
import org.dev.basic.service.SysParameterService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 系统参数 服务实现类
 * </p>
 *
 * @author dean.x.liu
 * @since 2021-05-19
 */
@Service
public class SysParameterServiceImpl extends ServiceImpl<SysParameterMapper, SysParameter> implements SysParameterService {

}

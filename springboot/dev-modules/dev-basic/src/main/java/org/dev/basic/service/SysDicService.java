package org.dev.basic.service;


import com.baomidou.mybatisplus.extension.service.IService;
import org.dev.basic.entity.SysDic;

/**
 * <p>
 * 数据库字典组 服务类
 * </p>
 *
 * @author dean.x.liu
 * @since 2020-07-05
 */
public interface SysDicService extends IService<SysDic> {

}

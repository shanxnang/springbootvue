package org.dev.basic.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.dev.basic.entity.SysUpload;

/**
 * <p>
 * 上传文件 Mapper 接口
 * </p>
 *
 * @author dean.x.liu
 * @since 2020-07-18
 */
public interface SysUploadMapper extends BaseMapper<SysUpload> {

}

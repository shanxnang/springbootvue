package org.dev.basic.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.dev.basic.entity.SysDic;
import org.dev.basic.mapper.SysDicMapper;
import org.dev.basic.service.SysDicService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 数据库字典组 服务实现类
 * </p>
 *
 * @author dean.x.liu
 * @since 2020-07-05
 */
@Service
public class SysDicServiceImpl extends ServiceImpl<SysDicMapper, SysDic> implements SysDicService {

}

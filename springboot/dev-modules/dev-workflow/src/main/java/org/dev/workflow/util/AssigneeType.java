package org.dev.workflow.util;

public interface AssigneeType {

    /**
     * 签核类型为用户
     */
    String USER = "user";
    /**
     * 签核类型为角色
     */
    String ROLE = "user";
    /**
     * 签核类型为职位
     */
    String JOB = "job";
    /**
     * 签核类型为自定义
     */
    String CUSTOM = "custom";
}

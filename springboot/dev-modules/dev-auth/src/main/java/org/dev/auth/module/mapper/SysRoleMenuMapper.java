package org.dev.auth.module.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.dev.auth.module.entity.SysRoleMenu;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author hlt
 * @since 2020-06-24
 */
public interface SysRoleMenuMapper extends BaseMapper<SysRoleMenu> {

}

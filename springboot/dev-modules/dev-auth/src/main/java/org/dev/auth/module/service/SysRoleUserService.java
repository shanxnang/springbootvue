package org.dev.auth.module.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.dev.auth.module.entity.SysRoleUser;

/**
 * <p>
 * 角色用户关系表 服务类
 * </p>
 *
 * @author hlt
 * @since 2020-06-24
 */
public interface SysRoleUserService extends IService<SysRoleUser> {

}

package org.dev.auth.module.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.dev.auth.module.entity.SysRoleMenu;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author hlt
 * @since 2020-06-24
 */
public interface SysRoleMenuService extends IService<SysRoleMenu> {

}

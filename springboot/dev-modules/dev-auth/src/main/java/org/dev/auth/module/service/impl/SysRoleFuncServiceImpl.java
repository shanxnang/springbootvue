package org.dev.auth.module.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.dev.auth.module.entity.SysRoleFunc;
import org.dev.auth.module.mapper.SysRoleFuncMapper;
import org.dev.auth.module.service.SysRoleFuncService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author hlt
 * @since 2020-06-24
 */
@Service
public class SysRoleFuncServiceImpl extends ServiceImpl<SysRoleFuncMapper, SysRoleFunc> implements SysRoleFuncService {

}
